require 'faker'

namespace :db do
  desc "Fill database with sample data"
  task :populate => :environment do
    #Rake::Task['db:reset'].invoke
    [Profile].each(&:delete_all)
    50.times do |n|
      name  = Faker::Company.name
      year = 1900+rand(111)
      Profile.create!(
                    :name => Faker::Company.name,
                    :found_year => 1900+rand(111),
                    :workers => rand(1111),
                    :status => "a",
                    :category_ids => ["18","21","30"],
                    :phone => Faker::PhoneNumber.cell_phone,
                    :short_description => Faker::Lorem.paragraph,
                    :full_description =>Faker::Lorem.paragraph(20),
                    :url => Faker::Internet.url(Faker::Internet.domain_word),
                    :requisites => Faker::Lorem.paragraph,
                    :address => "#{Faker::Address.country}, #{Faker::Address.state}, #{Faker::Address.city}"
                   )
    end
  end
end