module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | %s" % ENV['PROJECT_NAME']
    end
  end

  def nav_link(link_text, link_path)
  class_name = current_page?(link_path) ? 'active' : ''

  content_tag(:li, :class => class_name) do
    link_to link_text, link_path
  end
  end

  def user_profile
    profile = Profile.where(user_id: current_user).first
    unless profile.blank?
    menu = nav_link profile.name, profile
    else
      menu = nav_link 'Create', new_profile_path
    end
      
    raw menu
  end
end
