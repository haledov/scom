class Profile < ActiveRecord::Base
  has_and_belongs_to_many :categories
	belongs_to :user
  belongs_to :city
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
  attr_accessor :country
  attr_accessor :city
  attr_accessor :region
  has_attached_file :logo, 
  :path => ":rails_root/public/profiles/:attachment/:id/logo_:style.:extension",
  :url => "/profiles/:attachment/:id/logo_:style.:extension",
  source_file_options:  { all: '-background transparent' },
  :styles => {
  :thumb    => ['100x100#',  :jpg, :quality => 70],
  :preview  => ['480x480#',  :jpg, :quality => 70],
  :large    => ['600>',      :jpg, :quality => 70],
  :retina   => ['1200>',     :jpg, :quality => 30]
},
:convert_options => {
  :thumb    => '-set colorspace sRGB -strip',
  :preview  => '-set colorspace sRGB -strip',
  :large    => '-set colorspace sRGB -strip',
  :retina   => '-set colorspace sRGB -strip -sharpen 0x0.5'
}
validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png"]
before_save :sanitize_url
  # Scopes
  scope :active, lambda {
    where(status: 'a')
    .order("updated_at DESC")
  }
  scope :registred, lambda {
    where(status: 'r')
    .order("updated_at DESC")
  }
  scope :deleted, lambda {
    where(status: 'd')
    .order("updated_at DESC")
  }
  def self.in_categories(category_ids=[])
    return all if category_ids.blank? || category_ids.empty?
    includes(:categories).where(categories: { id: category_ids })
  end
  
  def sanitize_url
      unless self.url.include?("http://") || self.url.include?("https://")
        self.url = "http://" + self.url
      end
   end
   
  def should_generate_new_friendly_id?
    new_record?
  end
end
