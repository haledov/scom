$(document).on('ready page:load', function () {
    var addressPicker = new AddressPicker();

             $('#address').typeahead(null, {
                              displayKey: 'description',
                              source: addressPicker.ttAdapter()
              });
	 // Proxy inputs typeahead events to addressPicker
	addressPicker.bindDefaultTypeaheadEvent($('#address'))

	// Listen for selected places result
	$(addressPicker).on('addresspicker:selected', function (event, result) {
		$('#lat').val(result.lat());
	  	$('#lng').val(result.lng());
	  	$('#city').val(result.nameForType('locality'));
	  	$('#region').val(result.nameForType('administrative_area_level_1'));
	  	$('#country').val(result.nameForType('country'));
	  	$('#address').val(result.address());
	  	console.log(result.nameForType('locality'));
      	console.log(result.nameForType('administrative_area_level_1'));
	});


	

    
});
