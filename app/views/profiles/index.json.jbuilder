json.array!(@profiles) do |profile|
  json.extract! profile, :id, :name, :url, :phone, :place_id, :orgtype, :full_description, :short_description, :workers, :found_date
  json.url profile_url(profile, format: :json)
end
