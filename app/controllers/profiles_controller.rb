class ProfilesController < ApplicationController
  before_action :set_profile, only: [:edit, :update, :destroy]
  

  # GET /profiles
  # GET /profiles.json
  def index
    
    @profiles = Profile.in_categories(params[:profile][:category_ids]).active.page(params[:page]).per(10)
  end

  #GET /profiles/list/1
  def list
    <<-DOC
    if params[:category_id]
      @category_id = params[:category_id]
      @categories = Category.find(@category_id)
    else
      @categories = Category.roots
    end
    @profiles = @categories.profiles.active
    DOC

    
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    @profile = Profile.friendly.find(params[:id])
    if request.path != profile_path(@profile)
    redirect_to @profile, status: :moved_permanently
    end

  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
    
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = current_user.id

    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    params[:profile][:category_ids] ||= []
    @town = set_city
    params[:profile][:city_id] = @town.id
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    def set_profile
      @profile = Profile.where(user_id: current_user).first
      #@profile = Profile.find(params[:id])
    end

    def set_city

      c = Country.where(name: params[:profile][:country]).first_or_create
      r = Region.where(name: params[:profile][:region], country: c).first_or_create
      t = City.where(name: params[:profile][:city], country: c, region: r).first_or_create
      return t
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.permit(:category_ids => [])
      params.require(:profile).permit(:name, :logo, :country, :city, :requisites, :city_id, :region, :url, :phone, :place_id, :orgtype, :full_description, :short_description, :workers, :found_year, :lat, :lng, :address, :category_ids => [])
    end
end
