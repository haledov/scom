class RemoveLftAndRgtFromCategories < ActiveRecord::Migration
  def change
  	remove_column :categories, :lft
  	remove_column :categories, :rgt
  end
end
