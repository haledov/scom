class AddFoundYearToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :found_year, :integer
  end
end
