class AddCityIdToProfile < ActiveRecord::Migration
  def change
    add_reference :profiles, :city, index: true
    add_foreign_key :profiles, :cities
  end
end
