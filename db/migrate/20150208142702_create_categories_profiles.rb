class CreateCategoriesProfiles < ActiveRecord::Migration
  def change
    create_table :categories_profiles, :id => false do |t|
      t.references :category, :profile
    end

    add_index :categories_profiles, [:category_id, :profile_id],
      name: "categories_profiles_index",
      unique: true
  end
end
