class AddRequisitesToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :requisites, :text
  end
end
