class AddLatAndLngAndAddressToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :lat, :string
    add_column :profiles, :lng, :string
    add_column :profiles, :address, :string
  end
end
