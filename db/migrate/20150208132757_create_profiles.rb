class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :url
      t.string :phone
      t.integer :place_id
      t.integer :orgtype
      t.text :full_description
      t.text :short_description
      t.integer :workers
      t.date :found_date

      t.timestamps null: false
    end
  end
end
