Scom::Application.routes.draw do
  resources :cities

  resources :countries

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  resources :profiles
  resources :categories

  root "pages#home"
  get "home", to: "pages#home", as: "home"
  get "inside", to: "pages#inside", as: "inside"
  get "/contact", to: "pages#contact", as: "contact"
  post "/emailconfirmation", to: "pages#email", as: "email_confirmation"
  get "profiles/list/:category_id", to: "profiles#list", as: "profiles_list"
  get "profiles/:category_id", to: "profiles#list", as: "profiles_all"
  get "profile/:id", to: "profiles#show", as: "profile_show"
  get "posts", to: "pages#posts", as: "posts"
  get "posts/:id", to: "pages#show_post", as: "post"

  devise_for :users

  namespace :admin do
    root "base#index"
    get "posts/drafts", to: "posts#drafts", as: "posts_drafts"
    get "posts/dashboard", to: "posts#dashboard", as: "posts_dashboard"
    get "categories/dashboard", to: "categories#dashboard", as: "categories_dashboard"
    put "profiles/activate/:id", to: "profiles#activate", as: "profiles_activate"
    
    resources :users
    resources :posts
    resources :categories
    resources :profiles

  end

end
