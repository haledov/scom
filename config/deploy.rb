# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'danab'
set :repo_url, 'git@bitbucket.org:haledov/scom.git'

set :deploy_to, '/home/texer/apps/danab'

set :linked_files, %w{config/database.yml config/application.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end
  


  after :finishing, 'deploy:cleanup'
end
